*** Settings ***
Resource        ${CURDIR}/../keywords/import.robot
Library           OperatingSystem
Test Teardown   Close all browsers



*** Test Cases ***
TC-001 Verify that user can search product with contains keyword
    #1 Open Website Production
    common.Open browser and website
    
    #2 Search with keyword
    ${search_keyword}    ${expect_search_result}    ${Expect_product_description}      Retrieve testdata from excel
    search_product_features.Search product with keyword                 ${search_keyword}

    #3 Check Product in search Result
    result_pages.Verify search result page should be equal              ${expect_search_result}
    result_pages.Verify product in search result should be equal        ${Expect_product_description}

