*** Settings ***
Library         SeleniumLibrary
Library         String
Library         ExcelLibrary

#config
Variables       ${CURDIR}/../config/config.yaml

#common
Resource        common.robot

#features
Resource        ${CURDIR}/features/search_product_features.robot

#pages
Resource        ${CURDIR}/pages/dataexcel.robot
Resource        ${CURDIR}/pages/top_pages.robot
Resource        ${CURDIR}/pages/result_pages.robot
