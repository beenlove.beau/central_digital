*** Settings ***
Resource        ${CURDIR}/import.robot

*** Variables ***
${env}=     ${config['select_environment']} 


*** Keywords ***
Open browser and website
    IF     '${env}' == 'production'
            Log to console      Environment : production
            ${options}=         Evaluate  sys.modules['selenium.webdriver.chrome.options'].Options()    sys
            Call Method         ${options}    add_argument    --disable-notifications
            ${driver}=          Create Webdriver    Chrome    options=${options}
            Go To               url=${config['environment']['production']['url']}
            Maximize Browser Window
            top_pages.Close popup advertising
    END

Input text when it is visible
   [Arguments]                      ${locator}          ${text_input}  
    Wait until keyword succeeds     30s     1s          Input Text                      ${locator}          ${text_input}

Click element when it is visible
   [Arguments]                      ${locator}
    Wait until keyword succeeds     30s     1s          Click Element                   ${locator}    

Verify text should be equal
    [Arguments]                 ${result_text}          ${expect_text}
    Wait until keyword succeeds     30s     1s          Should be equal             ${result_text}           ${expect_text}


