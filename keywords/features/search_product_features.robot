*** Settings ***
Resource        ${CURDIR}/../import.robot


*** Keywords ***
Search product with keyword
    [Arguments]     ${search_keyword}
    top_pages.Input text for search     ${search_keyword}
    top_pages.Click search  