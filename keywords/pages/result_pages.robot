*** Settings ***
Resource        ${CURDIR}/../import.robot

*** Variables ***
${search_result_locator}=               xpath=//div[@class='_2oxKm']
${product_description_locator}=         xpath=//a[@data-product-id='OFM1009506']


*** Keywords ***
Verify search result page should be equal
    [Arguments]             ${expect_search_result}
    ${search_result_text}=         Wait until keyword succeeds     10s     1s          Get text        ${search_result_locator}
    Log to console          ---------------------------------------------------
    Log to console          Search Result Text : ${search_result_text}
    Log to console          Search Expect Text : ${expect_search_result}
    Log to console          ---------------------------------------------------
    common.Verify text should be equal   ${search_result_text}     ${expect_search_result}

Verify product in search result should be equal
    [Arguments]             ${expect_product_description}
    ${product_result_text}=         Wait until keyword succeeds     10s     1s          Get text        ${product_description_locator}
    Log to console          ---------------------------------------------------
    Log to console          product description Result Text : ${product_result_text}
    Log to console          product description Expect Text : ${expect_product_description}
    Log to console          ---------------------------------------------------
    common.Verify text should be equal   ${product_result_text}     ${expect_product_description}


