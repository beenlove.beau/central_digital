*** Settings ***
Resource        ${CURDIR}/../import.robot


*** Variables ***                  
${frame_popup}=                         xpath=//div[@classname='sp-fancybox-wrap sp-fancybox-wrap-10456 sp-advanced-css-10456']//iframe
${close_popup_button_locator}=          xpath=//div[@id='insider-notification-content']//i[contains(@id,'close-button')]
${input_search_locator}=                xpath=//input[@data-testid='txt-SearchBar']
${search_button_locator}=               xpath=//div[@id='btn-searchResultPage']


*** Keywords ***
Close popup advertising
    Wait until element is visible                   ${frame_popup}
    ${found_frame_popup}=      Run keyword and return status       Page should contain element     ${frame_popup}
    Log to console                              found_frame_popup : ${found_frame_popup} 

    IF      ${found_frame_popup}
        Select Frame	                            ${frame_popup}
        common.Click element when it is visible     ${close_popup_button_locator}
    END

Input text for search
    [Arguments]         ${search_keyword}
    Log to console                              Keyword Search : ${search_keyword} 
    common.Input text when it is visible        ${input_search_locator}              ${search_keyword}

Click search
    common.Click element when it is visible     ${search_button_locator}



