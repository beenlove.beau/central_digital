*** Settings ***
Resource        ${CURDIR}/../import.robot

*** Keywords ***
Retrieve testdata from excel
    ExcelLibrary.Open excel document            filename=..${/}testdata${/}testdata.xlsx                  doc_id=testdata
    ${xRowMaxTestdata}=     maxrow    testdata
    Log to console      Max Row : ${xRowMaxTestdata}
    FOR		${i}    IN RANGE   		1		${xRowMaxTestdata}
            Log to console      Start Loop : ${i} =================
            ${r}=    Evaluate           ${i} + 1
            ${search_keyword}                   Read Excel Cell          row_num=${r}      col_num=1      sheet_name=testdata
            ${expect_search_result}             Read Excel Cell          row_num=${r}      col_num=2      sheet_name=testdata
            ${expect_product_description}       Read Excel Cell          row_num=${r}      col_num=3      sheet_name=testdata

            Log to console      Row : ${r}
            Log to console      Search Keyword : ${search_keyword} 
            Log to console      search result text : ${expect_search_result} 
            Log to console      Product Desc : ${expect_product_description} 
            Log to console      End Loop : ${i} =================
    END

    [Return]        ${search_keyword}       ${expect_search_result}     ${expect_product_description}